First home work

Instruction

*Пройти Git How To: Guided Git Tutorial
*Поставить Git (выбрать опцию с Git Bash Enabled)
*Создать аккаунт на гитлабе (личная почта)
*Создать репозиторий на гитлабе, выкачать его
*Решить задачу описанную ниже и запушить код в репозиторий (не одним коммитом желательно)
Создать класс GreetingHandling, который будет удовлетворять следующим условиям:

1)Есть два класса Greeting и Handling. В каждом классе объявлено по одному конструктору и деструктору. В конструкторе Greeting печатается первая часть приветствия (по умолчанию "Hello"), в деструкторе - вторая (по умолчанию "World"). В конструкторе Handling печатается ", ", в деструкторе - "!".
2)В классе GreetingHandling не объявлено мемберов и методов, кроме конструктора без аргументов и конструктора от двух аргументов.
3)Динамическую алокацию использовать запрещено.
4)Нужно создать цепочку вызовов конструкторов и деструкторов таким образом, чтобы выводилось полное приветствие со знаками препинания (по умолчанию "Hello, World!") и чтобы класс GreetingHandling можно было использовать повторно.

Пример использования:
Input:
GreetingHandling gh1("Kanzi", "C++");
std::cout << "\n~~~~~~~~~~~~~~~~~~\n";
GreetingHandling gh2;
std::cout << "\n~~~~~~~~~~~~~~~~~~\n";
gh2 = GreetingHandling(std::string("Bye-bye"), std::string("everyone"));


Output:
Kanzi, C++!
~~~~~~~~~~~~~~~~~~
Hello, World!
~~~~~~~~~~~~~~~~~~
Bye-bye, everyone!